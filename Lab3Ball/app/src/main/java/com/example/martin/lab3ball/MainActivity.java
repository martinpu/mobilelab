package com.example.martin.lab3ball;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.ImageView;

/**
 * This class will take the inputs from movement sensors and move a ball on the screen.
 * If the ball hits the wall it will make a sound.
 * This call will contain all the Java code needed for the Lab3 in Mobile Development.
 * It extends Activity to implement SensorEventListener needed for the use of the sensors.
 * This will include the methods onSensorChanged and onAccuracyChanged.
 * Borrowed code and rewritten from:
 * https://stackoverflow.com/questions/6479637/android-accelerometer-moving-ball
 * and
 * https://developer.android.com/guide/topics/sensors/sensors_motion.html
 */
public class MainActivity extends Activity implements SensorEventListener {

    // Variables needed for the application, like the view and ImageView for the ball
    // and rectangle.
    public ImageView ball;
    public View rootView;

    // Variables to create boundaries of the ball in the rectangle.
    private int rectangleX;
    private int rectangleY;

    // Sensors needed for the application, this includes the sensorManager, Gravity, vibrate and
    // tone generator.
    public SensorManager sensorManager;
    public Sensor sensor;
    public Vibrator vibrator;
    public ToneGenerator toneGenerator;

    /**
     * Method to initialize on create (when application starts).
     * Overrides a method form android.app.Activity
     * @param savedInstanceState the instance state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // Setup the ImageView ball and the Shape rectangle.
        // Uses the android.R.id.content to find the root elements without id.
        this.rootView = findViewById(android.R.id.content);
        this.ball = findViewById(R.id.ballId);

        // Setup of the sensors, both SensorManager and the Sensor.
        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        // getDefaultSensor may return a nullException, this is prevented by the if statement.
        if (sensorManager != null) {
            this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        }
        this.vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        this.toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
    }

    /**
     * Method implemented by the SensorEventListener.
     * Used to find the balls position inside the rectangle and to make it move with collisions.
     * Overrides a method form android.hardware.SensorEventListener
     * @param sensorEvent sensor change event
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        // Find the position of the ball using getX() and getY().
        // getX() finds the x value of the frame.
        // getY() find the y value of the frame.
        float ballX = this.ball.getX();
        float ballY = this.ball.getY();

        // From the DOCS on https://developer.android.com/guide/topics/sensors/sensors_motion.html
        // sensorEvent.value[0] = Measured acceleration along the X axis without any bias compensation.
        // sensorEvent.value[1] = Measured acceleration along the Y axis without any bias compensation.
        float xAxis = sensorEvent.values[0];
        float yAxis = sensorEvent.values[1];

        // Set boundaries and movement speed of the ball. If the ball hits the wall it stops.
        // Also added sound if the ball hits the wall.
        if (ballX < 0) {
            this.ball.setX(ballX+15);
            sound();
            return;
        } else if (ballX > this.rectangleX) {
            this.ball.setX(ballX-15);
            sound();
            return;
        } else if (ballY < 0) {
            this.ball.setY(ballY+15);
            sound();
            return;
        } else if (ballY > this.rectangleY) {
            this.ball.setY(ballY-15);
            sound();
            return;
        }

        // Movement of the ball on the x and y axis of the screen. Uses the setY() and setX()
        // to set the values of the frame of x and y axis. updates every 0.5 frame.
        // This updated due to movement of the screen.
        if (xAxis > 0.5 || xAxis < -0.5) {
            this.ball.setY(ballY + xAxis);
        }
        if (yAxis > 0.5 || yAxis < -0.5) {
            this.ball.setX(ballX + yAxis);
        }
    }


    /**
     * Method implemented by the SensorEventListener.
     * Decided not to implement this method.
     * Overrides a method form android.hardware.SensorEventListener
     * @param sensor What sensor
     * @param i the accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    /**
     * Method needed for the resume of the application whenever you navigate to the activity
     * or something else.
     * Overrides a method form android.app.Activity
     * TO USE LAMBDA EXPRESSIONS (getViewTreeObserver) I HAD TO CHANGE THE COMPATIBILITY TO 1.8
     * File --> Project Structure --> app --> Source and Target Compatibility = 1.8
     * Tips and uses of this method taken from
     * https://stackoverflow.com/questions/6479637/android-accelerometer-moving-ball
     */
    @Override
    protected void onResume() {
        super.onResume();

        this.rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            this.rectangleX = this.rootView.getWidth() - 160;
            this.rectangleY = this.rootView.getHeight() - 160;

            this.ball.setX((float) rectangleX / 2);
            this.ball.setY((float) rectangleY / 2);

            this.sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_FASTEST);

     });
    }

    /**
     * Method needed for the stop of the application whenever you navigate from the activity
     * or something else.
     * Overrides a method form android.app.Activity
     * Tips and uses of this method taken from
     * https://stackoverflow.com/questions/6479637/android-accelerometer-moving-ball
     */
    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    /**
     * Method to add sound and vibrate to the applications if the wall hits the wall.
     * This method is called from the onSensorChanged, inside the boundaries.
     */
    private void sound() {
        this.vibrator.vibrate(100);
        this.toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 100);
    }

}
