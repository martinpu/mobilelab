package com.example.martin.lab4chat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashSet;


/**
 * Class to display all your online friends in the chat application. This class still displays
 * duplicates and I can't figure out how to get past it.
 */
public class Friends extends AppCompatActivity  {

    private FirebaseListAdapter<ChatMessage> adapter;

    HashSet<String> hset = new HashSet<>();

    /**
     * On create of Friends activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends);
        // To display the friends in the application.
        displayUsers();

    }

    /**
     * On Resume of the application, this is needed due to the startlistening method.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    /**
     * Display all the users that have posted a chat message.
     */
    private void displayUsers() {


        final ListView listFriends = findViewById(R.id.listFriends);


        Query query = FirebaseDatabase.getInstance().getReference().orderByChild("messageUser");

        FirebaseListOptions<ChatMessage> options = new FirebaseListOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .setLayout(R.layout.message)
                .build();

        adapter = new FirebaseListAdapter<ChatMessage>(options) {

            @Override
            protected void populateView(View v, ChatMessage model, int position) {

                // Get references to the views of message.xml
                TextView messageUser = v.findViewById(R.id.message_user);

                String s = model.getMessageUser();
                String[] array = new String[]{s};

                    for (String username : array) {
                    hset.add(username);
                       for (String temp : hset) {
                           messageUser.setText(temp);
                       }
                    }
            }
        };

        listFriends.setAdapter(adapter);
        listFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(Friends.this, UserMessages.class);
                String message = (String) listFriends.getItemAtPosition(position).toString();
                intent.putExtra("Username", message);
                startActivity(intent);
            }
        });
    }
}
