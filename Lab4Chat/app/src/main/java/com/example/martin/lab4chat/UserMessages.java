package com.example.martin.lab4chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashSet;

/**
 * Created by Martin on 27.03.2018.
 */

public class UserMessages extends AppCompatActivity{

    private FirebaseListAdapter<ChatMessage> adapter;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.user_messages);

            displayUserMessages();

            String newString;
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();
                if(extras == null) {
                    newString= null;
                } else {
                    newString= extras.getString("Username");
                }
            } else {
                newString= (String) savedInstanceState.getSerializable("Username");
            }
            Log.d("Test1", newString);
        }

        @Override
        protected void onResume() {
            super.onResume();
            if (adapter != null) {
                adapter.startListening();
            }
        }

    private void displayUserMessages() {


        ListView listOfMessages = findViewById(R.id.userMessages);

        Query query = FirebaseDatabase.getInstance().getReference();

        FirebaseListOptions<ChatMessage> options = new FirebaseListOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .setLayout(R.layout.message)
                .build();

        //adapter = new FirebaseListAdapter<ChatMessage>(this, ChatMessage.class,
        //       R.layout.message, FirebaseDatabase.getInstance().getReference()) {
        adapter = new FirebaseListAdapter<ChatMessage>(options) {

            @Override
            protected void populateView(View v, ChatMessage model, int position) {

                // Get references to the views of message.xml
                TextView messageText = v.findViewById(R.id.message_text);
                TextView messageTime = v.findViewById(R.id.message_time);

                // Set their text
                messageText.setText(model.getMessageText());
                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));
            }
        };

        listOfMessages.setAdapter(adapter);
    }
}
