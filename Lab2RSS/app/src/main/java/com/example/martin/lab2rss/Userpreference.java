package com.example.martin.lab2rss;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin Pukstad
 * Class in which the user is able to choose his or hers preferences on the search of RSS feeds.
 * The user will be able to choose between RSS2.0 and Atom RSS feed and will also be able to
 * choose how often the feed is updated and how many items that will be shown in the display.
 * The user also chooses the URL to the RSS feed wanted. This feed is default vg.no
 */

public class Userpreference extends AppCompatActivity {

    EditText userInput;
    Spinner spinner;
    Button saveBtn;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_preference);

        // Fill the different spinners.
        fillTypeSpinner();
        fillTimeSpinner();
        fillDisplaySpinner();

        // Get the default or stored value in the preferences on start.
        getPrefValues();

        // Save Button to save the changes made by the user in the preferences.
        saveBtn = findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
    }

    /**
     * Method to save the values chosen by the user in the dropdown menues and the textfield with
     * URL.
     */
    private void save() {
        int given;

        SharedPreferences.Editor pref = sharedpreferences.edit();

        spinner = findViewById(R.id.updateTypeSpinner);
        given = spinner.getSelectedItemPosition();
        pref.putInt("Type", given);

        spinner = findViewById(R.id.updateDisplaySpinner);
        given = spinner.getSelectedItemPosition();
        pref.putInt("Items", given);

        spinner = findViewById(R.id.updateTimeSpinner);
        given = spinner.getSelectedItemPosition();
        pref.putInt("Time", given);

        pref.putString("URL", userInput.getText().toString());
        pref.apply();

        Toast.makeText(getApplicationContext(), "Changes saved", Toast.LENGTH_LONG).show();
    }

    private void getPrefValues() {
        int stored;
        sharedpreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        userInput = findViewById(R.id.url);
        userInput.setText(sharedpreferences.getString("url", "https://www.vg.no/rss/feed/"));

        spinner = findViewById(R.id.updateTypeSpinner);
        stored = sharedpreferences.getInt("Type",0 );
        spinner.setSelection(stored);

        spinner = findViewById(R.id.updateDisplaySpinner);
        stored = sharedpreferences.getInt("Items",0 );
        spinner.setSelection(stored);

        spinner = findViewById(R.id.updateTimeSpinner);
        stored = sharedpreferences.getInt("Time",0 );
        spinner.setSelection(stored);
    }

    /**
     * Back button
      */
    public void back(View view) {
        Intent back = new Intent(Userpreference.this, Itemlist.class);
        back.putExtra("URL", getUrl());
        back.putExtra("Items", getDisplayCount());
        back.putExtra("Time", getTime());
        startActivity(back);
    }

    /**
     * Method to find the preferenced amount of items shown to the user, given by the user.
     * @return itme with the correct chosen value.
     */
    public int getDisplayCount() {
        int item;
        spinner = findViewById(R.id.updateDisplaySpinner);
        item = spinner.getSelectedItemPosition();

        switch (item) {
            case (1): item = 20; break;
            case (2): item = 50; break;
            case (3): item = 100; break;
            default: item = 10; break;
        }
        return item;
    }

    /**
     * Method to find the preferenced time between updated given by the user.
     * @return time with the correct chosen value.
     */
    public int getTime() {
        int time;
        spinner = findViewById(R.id.updateTimeSpinner);
        time = spinner.getSelectedItemPosition();

        switch (time) {
            case (1): time = 60000; break;
            case (2): time = 10000000; break;
            default: time = 10000; break;
        }
        return time;
    }

    /**
     * Method to fill the type spinner in which the user chooses what RSS feed should be used,
     * between RSS2.0 and Atom.
     */
    public void fillTypeSpinner() {
        List<String> typeArray = new ArrayList<>();

        // Items in the typeArray array.
        typeArray.add("RSS 2.0");
        typeArray.add("Atom");

        // Fill the updateTypeSpinner.
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, typeArray);

        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = findViewById(R.id.updateTypeSpinner);
        spinner.setAdapter(typeAdapter);

        spinner = null;
    }

    /**
     * Method to fill the spinner in which the user chooses how often the feed should be updated.
     * The user can choose between every 10 minute, 60 minute or every day.
     */
    public void fillTimeSpinner() {
        List<String> timeArray = new ArrayList<>();

        // Items in the timeArray array.
        timeArray.add("10 min");
        timeArray.add("60 min");
        timeArray.add("1 day");

        // Fill the updateTimeSpinner.
        ArrayAdapter<String> timeAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, timeArray);

        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = findViewById(R.id.updateTimeSpinner);
        spinner.setAdapter(timeAdapter);

        spinner = null;
    }

    /**
     * Method to fill the spinner which the user uses to choose how many items will be shown.
     * The user can choose between 10, 20, 50 or 100 items.
     */
    public void fillDisplaySpinner() {
        List<String> itemArray = new ArrayList<>();

        // Items in the itemArray array.
        itemArray.add("10");
        itemArray.add("20");
        itemArray.add("50");
        itemArray.add("100");

        // Fill the updateDisplaySpinner.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, itemArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = findViewById(R.id.updateDisplaySpinner);
        spinner.setAdapter(adapter);

        spinner = null;
    }

    /**
     * Method to get the URL given by the user.
     * @return the url set by the user, or the default vg.no.
     */
    public String getUrl () {
        return userInput.getText().toString();
    }
}
