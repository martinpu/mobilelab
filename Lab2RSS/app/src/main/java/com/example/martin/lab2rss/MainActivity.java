package com.example.martin.lab2rss;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * Class to view the content of the chosen RSS feed from the ListView in Itemlist.class
 * This should just open the chosen link in a webView.
 */
public class MainActivity extends AppCompatActivity {

    // Needed variables, this includes the URL gotten from the shared preferences
    // from the Userpreferences --> Itemlist --> MainActivity (to show the content)
    // Also need a WebView to show the content.
    String sUrl;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        sUrl = intent.getStringExtra("URL");

        // How to set up webView taken and modified from
        // https://developer.android.com/guide/webapps/webview.html
        // This includes the usage of javascriptenable.
        // Will ignore the warning of XSS vulnerability at this time.
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebClient());

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        webView.loadUrl(sUrl);

    }

    /**
     * Class taken from
     * https://developer.android.com/reference/android/webkit/WebViewClient.html
     * Is needed to provide for the webclient used in the application.
     */
    private class WebClient extends WebViewClient
    {
        public boolean shouldOverrideUrlLoading(WebView view,String sUrl)
        {
            view.loadUrl(sUrl);
            return true;
        }
    }
}
