package com.example.martin.lab2rss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Martin
 * Class to display the list of feeds taken from the default or given URL by the user.
 * The user is also able to close the application, go to my preferences or force a fetch to
 * update the feed given at any given time.
 */

public class Itemlist extends AppCompatActivity {

    private Handler fetch = new Handler();

    ListView list;
    URL url;

    String sUrl;
    int displayCount;
    int displayTime;

    ArrayList<String> titles;
    ArrayList<String> links;

    // To get the values selected by the user in preferences.
    // Usage taken from https://www.tutorialspoint.com/android/android_shared_preferences.htm
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.itemlist);

        getPreferences();

        // Open an intent with the default values.
        Intent open = getIntent();
        sUrl = open.getStringExtra("URL");
        Toast.makeText(getApplicationContext(), sUrl, Toast.LENGTH_LONG).show();
        displayCount = open.getIntExtra("Items", 10);
        displayTime = open.getIntExtra("Time", 10000);

        // Check if the local variable sUrl is empty, and if so fill it
        if (sUrl == null) {
            sUrl = getUrl();
        }

        // Build the view in the ListView in Itemlist to show all the RSS feeds.
        list = findViewById(R.id.listItem);
        titles = new ArrayList<>();
        links  = new ArrayList<>();
        // ClickListener for items clicked in the RSS feed. This will open the MainActivity(WebView)
        // and show the content of the chosen RSS feed.
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String link = links.get(i);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("URL", link);
                startActivity(intent);
            }
        });

        // Run the feed.
        Feed.run();
    }

    /**
     * Method to run the feed and run in background with the correct update time given by
     * the user in preferences.
     */
    private Runnable Feed = new Runnable()
    {
        @Override
        public void run()
        {
            new FetchFeedTask().execute();
            fetch.postDelayed(this, displayTime);
        }
    };

    /**
     * Method to fill the url if empty. This is gotten by the SharePreferences from
     * Userpreference.class
     * @return the url given from userpreference.
     */
    public String getUrl() {
        SharedPreferences theUrl = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return theUrl.getString("URL","https://www.vg.no/rss/feed/");
    }

    private void getPreferences () {
        sharedpreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
    }

    /**
     * Button to close the application when Back is pressed.
     * @param view for the button
     */
    public void back(View view) {
        finish();
    }

    /**
     * Button to move to my preferences. This is where the user is able to choose different
     * preferences on his/her search of a RSS feed.
     * @param view for the button
     */
    public void myPref(View view) {
        Intent intent = new Intent(this, Userpreference.class);

        startActivity(intent);
    }

    /**
     * Button to force a fetch on the listView instead of waiting for the chosen length in
     * preferences.
     * @param view for the button
     */
    public void fetch(View view) {
        Feed.run();
    }

    /**
     * Method to fetch feed task, this is taken and modified from tutorial on RSS feed in Java.
     * https://www.androidauthority.com/simple-rss-reader-full-tutorial-733245/
     */
    public class FetchFeedTask extends AsyncTask<Integer, Integer, Exception>
    {
        ProgressDialog progressDialog = new ProgressDialog(Itemlist.this);
        Exception exc = null;
        @Override

        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.setMessage("Finding all the good feeds...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Exception s)
        {
            super.onPostExecute(s);

            ArrayAdapter<String> adapter= new ArrayAdapter<>(
                    Itemlist.this, android.R.layout.simple_list_item_1, titles);
            list.setAdapter(adapter);

            progressDialog.dismiss();
        }

        @Override
        protected Exception doInBackground(Integer... integers)
        {
            try
            {
                if(sUrl==null)
                {
                    url = new URL("https://www.vg.no/rss/feed/");
                }
                else
                {
                    url = new URL(sUrl);
                }

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

                factory.setNamespaceAware(false);

                XmlPullParser xpp = factory.newPullParser();

                xpp.setInput(getInputStream(url), "UTF_8");

                boolean insideItem = false;

                int eventType = xpp.getEventType();

                while(eventType != XmlPullParser.END_DOCUMENT && links.size()<displayCount)
                {
                    if(eventType == XmlPullParser.START_TAG)
                    {
                        if(xpp.getName().equalsIgnoreCase("item"))
                        {
                            insideItem = true;
                        }
                        else if(xpp.getName().equalsIgnoreCase("title"))
                        {
                            if(insideItem)
                            {
                                titles.add(xpp.nextText());
                            }
                        }
                        else if ( xpp.getName().equalsIgnoreCase("link"))
                        {
                            if(insideItem)
                            {
                                links.add(xpp.nextText());
                            }
                        }
                    }
                    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase(
                            "item"))
                    {
                        insideItem= false;
                    }
                    eventType = xpp.next();
                }
            }catch (MalformedURLException e)
            {
                exc = e;
            }catch (XmlPullParserException e)
            {
                exc = e;
            }catch(IOException e)
            {
                exc = e;
            }
            return exc;
        }
    }

    public InputStream getInputStream(URL url)
    {
        try
        {
            return url.openConnection().getInputStream();
        } catch (IOException e)
        {
            return null;
        }
    }

}
